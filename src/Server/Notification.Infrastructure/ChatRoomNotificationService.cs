﻿using Domain.Abstractions.Notifications;
using Domain.Entities;
using Microsoft.AspNetCore.SignalR;
using Notification.Infrastructure.Hubs;

namespace Notification.Infrastructure;

public class ChatRoomNotificationService: IChatRoomNotification
{
    private readonly IHubContext<ChatRoomNotificationHub> _hubContext;
    
    public ChatRoomNotificationService(IHubContext<ChatRoomNotificationHub> hubContext)
    {
        _hubContext = hubContext;
    }
    
    public async Task SendMessage(ChatRoom chatRoom, string message, string method = "TextMessage")
    {
        await _hubContext.Clients.Group(chatRoom.Name).SendAsync(method, message);
    }

    public async Task UserJoin(ChatRoom chatRoom, User user)
    {
        await _hubContext.Groups.AddToGroupAsync(user.ConnectionId, chatRoom.Name);

        await SendMessage(chatRoom, $"{user.UserName} has joined the group {chatRoom.Name}.","UserJoinChatRoom");
    }

    public async Task UserLeft(ChatRoom chatRoom, User user)
    {
        await _hubContext.Groups.RemoveFromGroupAsync(user.ConnectionId, chatRoom.Name);

        await SendMessage(chatRoom, $"{user.UserName} has left the group {chatRoom.Name}.", "UserJoinChatRoom");
    }
}