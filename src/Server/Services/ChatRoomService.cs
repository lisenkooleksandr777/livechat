using Domain.Abstractions.Notifications;
using Domain.Abstractions.Repositories;
using Domain.Entities;

namespace Services;

public class ChatRoomService
{
    private readonly IUserRepository _userRepository;
    private readonly IChatRoomRepository _chatRoomRepository;
    private readonly IChatRoomNotification _chatRoomNotification;

    public ChatRoomService(IUserRepository userRepository,
        IChatRoomRepository chatRoomRepository,
        IChatRoomNotification chatRoomNotification)
    {
        _userRepository = userRepository;
        _chatRoomRepository = chatRoomRepository;
        _chatRoomNotification = chatRoomNotification;
    }

    public async Task<ChatRoom> CreateChatRoom(string userName, string chatName)
    {
        var user = await _userRepository.GetUser(userName);
        if (user == null)
        {
            throw new Exception("Non existing user action");
        }

        var chatRoom = new ChatRoom()
        {
            Name = chatName,
            Owner = user,
        };

        var c = await _chatRoomRepository.Create(chatRoom);
        c.JoinRoom(user);
        user.JoinChat(c);

        await _chatRoomNotification.UserJoin(c, user);
        
        return c;
    }

    public async Task<List<ChatRoom>> GetChatRooms()
    {
        return await _chatRoomRepository.GetChatRooms();
    }

    public async Task<ChatRoom> JoinChatRoom(string userName, string chatName)
    {
        var user = await _userRepository.GetUser(userName);
        if (user == null)
        {
            throw new Exception("Non existing user action");
        }

        var c = await _chatRoomRepository.Get(chatName);
        if (c == null)
        {
            throw new Exception("Non existing chat action");
        }

        c.JoinRoom(user);
        user.JoinChat(c);

        await _chatRoomNotification.UserJoin(c, user);

        return c;
    }

    public async Task<List<ChatRoom>> LeaveChatRoom(string userName, string chatName)
    {
        var user = await _userRepository.GetUser(userName);
        if (user == null)
        {
            throw new Exception("Non existing user action");
        }

        var c = await _chatRoomRepository.Get(chatName);
        c.LeaveRoom(user);
        user.LeaveChat(c);

        await _chatRoomNotification.UserLeft(c, user);

        return await GetChatRooms();
    }

    public async Task<Message> SendMessage(string userName, string chatName, string message)
    {
        var user = await _userRepository.GetUser(userName);
        if (user == null)
        {
            throw new Exception("Non existing user action");
        }

        var c = await _chatRoomRepository.Get(chatName);
        if (c == null)
        {
            throw new Exception("Non existing chat action");
        }

        var m = new Message()
        {
            Text = message,
            Author = user,
            DateSent = DateTime.UtcNow
        };

        var msg = await c.SendMessage(m);

        await _chatRoomNotification.SendMessage(c, $"{msg.Author.UserName} saying: msg.Text at {msg.DateSent}");

        return msg;
    }
}