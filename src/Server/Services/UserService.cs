using Domain.Abstractions.Repositories;
using Domain.Entities;

namespace Services;

public class UserService
{
    private readonly IUserRepository _userRepository;
    
    public UserService(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }
    
    public async Task<User> NewUser(string userName, string connectionId)
    {
        var u = new User()
        {
            UserName = userName,
            ConnectionId = connectionId,
        };

        return await _userRepository.CreateUser(u);
    }
}