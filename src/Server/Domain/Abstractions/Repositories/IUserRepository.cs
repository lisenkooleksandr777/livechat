using Domain.Entities;

namespace Domain.Abstractions.Repositories;

public interface IUserRepository
{
    public Task<User> CreateUser(User user);
    public Task<User> GetUser(string userName);
}