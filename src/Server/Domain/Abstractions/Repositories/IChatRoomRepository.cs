using Domain.Entities;

namespace Domain.Abstractions.Repositories;

public interface IChatRoomRepository
{
    public Task<ChatRoom> Create(ChatRoom chatRoom);

    public Task<ChatRoom> Get(string chatRoomName);

    public Task<List<ChatRoom>> GetChatRooms();
}