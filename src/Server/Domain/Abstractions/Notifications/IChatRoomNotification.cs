using Domain.Entities;

namespace Domain.Abstractions.Notifications;

public interface IChatRoomNotification
{
    public Task SendMessage(ChatRoom chatRoom, string message, string method = "TextMessage");
    
    public Task UserJoin(ChatRoom chatRoom, User user);
    
    public Task UserLeft(ChatRoom chatRoom, User user);
}