namespace Domain.Entities;

public class ChatRoom
{
    public string Name { get; set; }

    public User Owner { get; set; }

    public ICollection<Message> Messages { get; set; } = new List<Message>();
    public ICollection<User> Participants { get; set; } = new List<User>();
    
    public void JoinRoom(User user)
    {
        if (Participants.All(x => x.UserName != user.UserName))
        {
            Participants.Add(user);
        }
    }

    public void LeaveRoom(User user)
    {
        var u = Participants.FirstOrDefault(x => x.UserName == user.UserName);

        if (u != null)
        {
            Participants.Remove(u);
        }
    }

    public Task<Message> SendMessage(Message message)
    {
        Messages.Add(message);

        return Task.FromResult(message);
    }
}