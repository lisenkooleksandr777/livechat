namespace Domain.Entities;

public class User
{
    public string UserName { get; set; }

    public string ConnectionId { get; set; }

    private ICollection<ChatRoom> ChatRooms { get; set; } = new List<ChatRoom>();
    
    public void JoinChat(ChatRoom chatRoom)
    {
        if (ChatRooms.All(x => x.Name != chatRoom.Name))
        {
            ChatRooms.Add(chatRoom);
        }
    }
    
    public void LeaveChat(ChatRoom chatRoom)
    {
        var chat = ChatRooms.FirstOrDefault(x => x.Name == chatRoom.Name);

        if (chat != null)
        {
            ChatRooms.Remove(chat);   
        }
    }
}