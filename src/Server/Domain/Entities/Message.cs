namespace Domain.Entities;

public class Message
{
    public string Text { get; set; }

    public DateTime DateSent { get; set; }

    public User Author { get; set; }
}