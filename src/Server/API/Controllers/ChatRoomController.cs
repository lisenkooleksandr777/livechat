using API.DTOs;
using Domain.Abstractions.Notifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Services;
using Storage.Infrastructure;

namespace API.Controllers;

[Route("chat-rooms")]
[ApiController]
public class ChatRoomController: ControllerBase
{
    private readonly ChatRoomService _chatRoomService;

    public ChatRoomController(ChatRoomService chatRoomService)
    {
        _chatRoomService = chatRoomService;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var chatRooms = await _chatRoomService
            .GetChatRooms();

        return Ok(chatRooms);
    }
    
    [HttpPost]
    public async Task<IActionResult> CreateChatRoom([FromBody] ChatRoomFormDto room)
    {
        var chatRoom = await _chatRoomService
            .CreateChatRoom(room.UserName, room.ChatName);

        return Ok(chatRoom);
    }
    

    [HttpPut("{chatName}/join")]
    public async Task<IActionResult> JoinChatRoom(string chatName, [FromBody] UserDto user)
    {
        var chatRoom = await _chatRoomService
            .JoinChatRoom(user.UserName, chatName);

        return Ok(chatRoom);
    }
    
    [HttpPut("{chatName}/leave")]
    public async Task<IActionResult> LeaveChatRoom(string chatName, [FromBody] UserDto user)
    {
        var chatRoom = await _chatRoomService
            .LeaveChatRoom(user.UserName, chatName);

        return Ok(chatRoom);
    }

    [HttpPost("{chatName}/messages")]
    public async Task<IActionResult> SendMessage(string chatName, [FromBody] MessageDto message)
    {
        var chatRoom = await _chatRoomService
            .SendMessage(message.UserName, chatName, message.Text);

        return Ok(chatRoom);
    }
}