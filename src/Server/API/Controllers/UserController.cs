using API.DTOs;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace API.Controllers;

[Route("users")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly UserService _userService;

    public UserController(UserService userService)
    {
        _userService = userService;
    }

    [HttpPost]
    public async Task<IActionResult> CreateUser([FromBody] UserDto user)
    {
        var upsertUser = await _userService.NewUser(user.UserName, user.ConnectionId);

        return Ok(upsertUser);
    }
}