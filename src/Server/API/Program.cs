﻿using Domain.Abstractions.Notifications;
using Domain.Abstractions.Repositories;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Notification.Infrastructure;
using Notification.Infrastructure.Hubs;
using Services;
using Storage.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

#region DI
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSignalR();

builder.Services.TryAddScoped<IUserRepository, UsersRepository>();
builder.Services.TryAddScoped<IChatRoomRepository, ChatRoomRepository>();
builder.Services.TryAddScoped<IChatRoomNotification, ChatRoomNotificationService>();

builder.Services.TryAddScoped<ChatRoomService>();
builder.Services.TryAddScoped<UserService>();

#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.MapControllers();

app.MapHub<ChatRoomNotificationHub>("/push-notifications");

await app.RunAsync();