namespace API.DTOs;

public record MessageDto
{
    public string Text { get; set; }

    public string UserName { get; set; }
}