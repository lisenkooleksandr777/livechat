using Domain.Abstractions.Repositories;
using Domain.Entities;

namespace Storage.Infrastructure;

public class UsersRepository : IUserRepository
{
    private static readonly Dictionary<string, User> Users = new();

    public Task<User> CreateUser(User user)
    {
        if (!Users.ContainsKey(user.UserName))
        {
            Users.Add(user.UserName, user);
        }
        
        return Task.FromResult(Users[user.UserName]);
    }
    
    public Task<User> GetUser(string userName)
    {
        if (!Users.ContainsKey(userName))
        {
            return Task.FromResult<User>(null);
        }

        return Task.FromResult(Users[userName])!;
    }
}