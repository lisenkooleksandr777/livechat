using Domain.Abstractions.Repositories;
using Domain.Entities;

namespace Storage.Infrastructure;

public class ChatRoomRepository: IChatRoomRepository
{
    private static readonly Dictionary<string, ChatRoom> ChatRooms = new();
    
    public Task<ChatRoom> Create(ChatRoom chatRoom)
    {
        if (!ChatRooms.ContainsKey(chatRoom.Name))
        {
            ChatRooms.Add(chatRoom.Name, chatRoom);
        }

        var room = ChatRooms[chatRoom.Name];

        return Task.FromResult(room);
    }

    public Task<ChatRoom> Get(string chatRoomName)
    {
        if (!ChatRooms.ContainsKey(chatRoomName))
        {
            throw new Exception("Chat room not found");
        }

        var room = ChatRooms[chatRoomName];

        return Task.FromResult(room);
    }

    public Task<List<ChatRoom>> GetChatRooms()
    {
        var rooms = ChatRooms.Select(x => x.Value).ToList();

        return Task.FromResult(rooms);
    }
}