namespace API.DTOs;

public record ChatRoomDto
{
    public string Name { get; set; }
}