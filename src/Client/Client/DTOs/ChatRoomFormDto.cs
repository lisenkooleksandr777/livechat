namespace API.DTOs;

public class ChatRoomFormDto
{
    public string UserName { get; set; }

    public string ChatName { get; set; }
}