namespace API.DTOs;

public record UserDto
{
    public string UserName { get; set; }

    public string ConnectionId { get; set; }
}