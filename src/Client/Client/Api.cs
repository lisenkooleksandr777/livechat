using API.DTOs;
using Flurl;
using Flurl.Http;

namespace ClientChat;

public static class Api
{
    private const string BasePath = "http://localhost:5093";

    public static async Task<string> GetChatRooms()
    {
        return await BasePath
            .AppendPathSegment("chat-rooms")
            .GetStringAsync();
    }
    
    public static async Task<string> CreateChatRoom(ChatRoomFormDto chatRoomFormDto)
    {
        return await BasePath
            .AppendPathSegment("chat-rooms")
            .PostJsonAsync(chatRoomFormDto)
            .ReceiveString();
    }
    
    public static async Task<string> JoinChatRoom(string chatName, UserDto user)
    {
        return await BasePath
            .AppendPathSegment("chat-rooms")
            .AppendPathSegment(chatName)
            .AppendPathSegment("join")
            .PutJsonAsync(user)
            .ReceiveString();
    }
    
    public static async Task<string> LeaveChatRoom(string chatName, UserDto user)
    {
        return await BasePath
            .AppendPathSegment("chat-rooms")
            .AppendPathSegment(chatName)
            .AppendPathSegment("leave")
            .PutJsonAsync(user)
            .ReceiveString();
    }

    public static async Task<string> MessageToChatRoom(string chatName, MessageDto message)
    {
        return await BasePath
            .AppendPathSegment("chat-rooms")
            .AppendPathSegment(chatName)
            .AppendPathSegment("messages")
            .PostJsonAsync(message)
            .ReceiveString();
    }
    
    public static async Task<string> NewUser(UserDto user)
    {
        return await BasePath
            .AppendPathSegment("users")
            .PostJsonAsync(user)
            .ReceiveString();
    }
}