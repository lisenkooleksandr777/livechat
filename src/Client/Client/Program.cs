﻿using API.DTOs;
using ClientChat;
using Microsoft.AspNetCore.SignalR.Client;

await MainClient.ExecuteAsync();

namespace ClientChat
{
    public static class MainClient
    {
        const string CreateUserCommand = "user_create"; //user_create username

        const string GetChatRoomsCommand = "chat_rooms_get"; //chat_rooms_get

        const string CreateChatRoomCommand = "chat_rooms_create"; //chat_rooms_create chat_name

        const string JoinChatRoomCommand = "chat_rooms_join"; //chat_rooms_join chat_name

        const string LeaveChatRoomCommand = "chat_rooms_leave"; //chat_rooms_leave chat_name

        const string MessageChatRoomCommand = "chat_rooms_message"; //chat_rooms_message chat_name message_text

        private static string _userName = null;
        private static string _connectionId = null;

        public static async Task ExecuteAsync()
        {
            #region CallBacks

            var uri = "https://localhost:7101/push-notifications";

            await using var connection = new HubConnectionBuilder().WithUrl(uri).Build();

            connection.On<string>("TextMessage", 
                (message) => { Print($"Push from TextMessage - {message}"); });

            connection.On<string>("UserJoinChatRoom",
                (message) => { Print($"Push from UserJoinChatRoom - {message}"); });

            connection.On<string>("UserLeftChatRoom",
                (message) => { Print($"Push from UserLeftChatRoom - {message}"); });

            await connection.StartAsync();
            _connectionId = connection.ConnectionId!;

            //Console.WriteLine(await Api.NewUser(new UserDto() { UserName = "user1", ConnectionId = _connectionId}));
            //Console.WriteLine(await Api.CreateChatRoom(new ChatRoomFormDto(){UserName = "user1", ChatName = "room1"}));
            //Console.WriteLine(await Api.JoinChatRoom("room1", new UserDto() { UserName = "user1", ConnectionId = _connectionId}));

            #endregion

            while (true)
            {
                var command = Console.ReadLine();
                var arguments = command?.Split(' ');
                switch (arguments?[0])
                {
                    case CreateUserCommand:
                        _userName = arguments[1];
                        Console.WriteLine(await Api.NewUser(new UserDto()
                            { UserName = arguments[1], ConnectionId = _connectionId }));
                        break;
                    case GetChatRoomsCommand:
                        Console.WriteLine(await Api.GetChatRooms());
                        break;
                    case CreateChatRoomCommand:
                        Console.WriteLine(await Api.CreateChatRoom(new ChatRoomFormDto()
                            { UserName = _userName, ChatName = arguments[1] }));
                        break;
                    case JoinChatRoomCommand:
                        Console.WriteLine(await Api.JoinChatRoom(arguments[1],
                            new UserDto() { UserName = _userName, ConnectionId = _connectionId }));
                        break;
                    case LeaveChatRoomCommand:
                        Console.WriteLine(await Api.LeaveChatRoom(arguments[1],
                            new UserDto() { UserName = _userName, ConnectionId = _connectionId }));
                        break;
                    case MessageChatRoomCommand:
                        Console.WriteLine(await Api.MessageToChatRoom(arguments[1],
                            new MessageDto() { UserName = _userName, Text = arguments[2] }));
                        break;
                    default:
                        Console.WriteLine(command);
                        break;
                }

                Console.WriteLine("-----------------------------");
            }
        }

        private static void Print(string message)
        {
            Console.WriteLine(message);
        }
    }
}